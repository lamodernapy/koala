<!doctype html>
<html>	
	<?php include('inc/head.php');?>
	<body>
        <?php include('inc/nav.php');?>
		<div class="container padding-v--big">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="title-dark--xl">Servicio Técnico</h2>
                <span class="division">&nbsp;</span>
                <p>Ante cualquier inconveniente con uno de nuestros productos, no dude en escribirnos.</p>
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <label>Nombre y Apellido</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label>CI cliente</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <label>Teléfono / Celular</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label>Email</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <label>N° Factura</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label>Cód. de Trazabilidad</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <label>Producto</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label>Motivo</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <label>Comentarios adicionales</label>
                                <textarea class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" value="Enviar" class="btn btn-primary pull-right">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php include('inc/footer.php');?>
        
    
	</body>
</html>