    <!doctype html>
    <html>	
    	<?php include('inc/head.php');?>
    	<body>
            <?php include('inc/nav.php');?>
    		<div id="map"></div>
            <div class="map-controller hidden-sm hidden-xs">
                <div class="map-header">
                    <h3 class="title-dark--lg">Encontrá la sucursal más cercana</h3>
                    <form class="margin-b--xs">
                        <select>
                            <option>Seleccioná una ciudad</option>
                            <option>Asunción</option>
                            <option>Fernando de la Mora</option>
                            <option>San Lorenzo</option>
                            <option>Luque</option>
                            <option>Ciudad del Este</option>
                            <option>Encarnación</option>
                        </select>
                    </form>
                    <p class="text-italic--xs">Hacé click en una sucursal de la lista para verla en el mapa</p>
                </div>
                <div class="map-list">
                    <div class="map-list--item active">
                        <h3 class="title-dark--sm">Vista Alegre</h3>
                        <p>Av. Eusebio Ayala 695 c/Capitán Rivarola<br>Tel: 0984 - 935 355</p>
                    </div>
                    <div class="map-list--item">
                        <h3 class="title-dark--sm">Vista Alegre</h3>
                        <p>Av. Eusebio Ayala 695 c/Capitán Rivarola<br>Tel: 0984 - 935 355</p>
                    </div>
                    <div class="map-list--item">
                        <h3 class="title-dark--sm">Vista Alegre</h3>
                        <p>Av. Eusebio Ayala 695 c/Capitán Rivarola<br>Tel: 0984 - 935 355</p>
                    </div>
                    <div class="map-list--item">
                        <h3 class="title-dark--sm">Vista Alegre</h3>
                        <p>Av. Eusebio Ayala 695 c/Capitán Rivarola<br>Tel: 0984 - 935 355</p>
                    </div>
                </div>
            </div>
            <?php include('inc/footer.php');?>
            
            <script>
              function initMap() {
                var uluru = {lat: -25.293887, lng: -57.617661};
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 15,
                  center: uluru
                });
                var marker = new google.maps.Marker({
                  position: uluru,
                  map: map
                });
              }
            </script>
            <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_Kctzva21aaMOwiaHQviXw5dVTgJ7aGI&callback=initMap">
            </script>
    	</body>
    </html>