<!doctype html>
<html>	
	<?php include('inc/head.php');?>
	<body>
        <?php include('inc/nav.php');?>
		<div class="container padding-v--big">
            <div class="col-md-6">
                <div class="cycle-slideshow" 
                    data-cycle-swipe=true
                    data-cycle-swipe-fx=scrollHorz
                    data-cycle-timeout=0
                    >

                    <div class="cycle-pager"></div>

                    <img src="assets/img/producto1.png" />
                    <img src="assets/img/producto2.png" />
                </div>
            </div>
            <div class="col-md-6">
                <h2 class="title-dark--xl">Sommier Ultrafirme</h2>
                <p>La Ultrafirme de Koala fue diseñada para personas que desean una sensación de firmeza extrema y a la vez alta durabilidad. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
                <h4 class="title-dark--sm">Características</h4>
                <ul class="inline-items margin-b--sm">
                    <li><img src="/assets/icons/SUAVE_RGB.png" width="50"></li>
                    <li><img src="/assets/icons/icono-pocket.png" width="50"></li>
                </ul>
                <h4 class="title-dark--sm">Seleccioná una medida</h4>
                <ul class="inline-items margin-b--sm">
                    <li><a href="#" class="item-bordered active">1,40 x 1,90 mts</a></li>
                    <li><a href="#" class="item-bordered">1,60 x 2,00 mts</a></li>
                    <li><a href="#" class="item-bordered">1,80 x 2,00 mts</a></li>
                </ul>
                <h4 class="title-dark--sm">Colores disponibles</h4>
                <ul class="inline-items margin-b--sm">
                    <li><a href="#" class="item-color item-blue active">&nbsp;</a></li>
                    <li><a href="#" class="item-color item-gray">&nbsp;</a></li>
                    <li><a href="#" class="item-color item-white">&nbsp;</a></li>
                </ul>
                <h4 class="title-dark--sm">Cantidad</h4>
                <div class="margin-b--sm">
                    <a id="down" href="javascript:void(0)" onclick="updateSpinner(this);" class="input-spinner--handler">-</a><input id="content" value="0" type="text" class="input-spinner--medium"/><a id="up" href="javascript:void(0)"  onclick="updateSpinner(this);" class="input-spinner--handler">+</a>
                </div>
                <ul class="inline-items product-actions">
                    <li><span class="product-price"><small>Contado</small><br><strong>Gs.2.300.000</strong></span><rb</li>
                    <li><a href="#" class="btn btn-secondary hidden-sm hidden-xs">Calculá tu cuota</a></li>
                    <li><a href="#" class="btn btn-primary hidden-sm hidden-xs">Solicitá un asesor de ventas</a></li>
                    <li><a href="#" class="btn btn-secondary btn-sm visible-sm visible-xs">Calculá tu cuota</a></li>
                    <li><a href="#" class="btn btn-primary btn-sm visible-sm visible-xs">Solicitá un asesor de ventas</a></li>
                </ul>
                <ul class="inline-items">
                    <li><a href="solicitar-asesor.php" class="tooltip-trigger" style="padding: 12px 13px;"><img src="/assets/icons/asesor-icon.png">Solicitar asesor</a></li>
                    <li><button type="button" class="tooltip-trigger" data-toggle="tooltip" data-placement="bottom" title="<ul><li>-Copia de C.I. actual</li><li>- Certificado laboral /Liquidación de salario 3 últimos o liquidación del IVA 3 últimas</li><li>- 2 referencias comerciales</li><li>- 2 referencias personales</li><li>- Ingreso del salario mínimo vigente</li><li>- Informconf Limpio.</li></ul>" data-html="true" style="text-align:left;">
                    <img src="/assets/icons/credito-icon.png">Requisitos para crédito</button></li>
                </ul>
            </div>
        </div>
        <div class="container padding-v--big">
            <div class="division">&nbsp;</div>
            <div class="col-md-12">
                <h3 class="title-dark--xl margin-b--sm">Te puede interesar</h3>
            </div>
            <div class="col-md-3 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                        <span class="promo-badge">20% OFF</span>
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-3 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-3 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-3 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
        </div>
        <?php include('inc/footer.php');?>
        
    
	</body>
</html>