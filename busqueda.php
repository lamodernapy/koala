    <!doctype html>
    <html>	
    	<?php include('inc/head.php');?>
    	<body>
            <?php include('inc/nav.php');?>
            <div class="search">
                <div class="container padding-v--big">
                    <div class="col-md-12 margin-b--sm">
                        <form>
                            <div class="col-md-12">
                        <h2 class="title-dark--xl">Encontrá el producto que necesitás</h2></div>
                            <div class="col-md-3 margin-b--sm">
                                <div class="input-group">
                                    <label>Palabras Clave</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3 margin-b--sm">
                                <div class="input-group">
                                    <label>Categoría</label>
                                    <select>
                                        <option>Sommier</option>
                                        <option>Accesorios</option>
                                        <option>Blanquería</option>
                                        <option>Living</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 margin-b--sm">
                                <div class="input-group">
                                    <label>Cuotas</label>
                                    <select>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 margin-b--sm">
                                <div class="input-group">
                                    <label>Monto máximo por cuota</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label><strong>Características</strong></label>
                                <ul class="inline-items">
                                    <li>
                                        <input type="checkbox" id="" name="" value="newsletter">
                                        <label for="">Anti-ácaros</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="" name="" value="newsletter">
                                        <label for="">Espuma viscolástica</label>
                                    </li> 
                                    <li>
                                        <input type="checkbox" id="" name="" value="newsletter">
                                        <label for="">Tecnología Pocket</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="" name="" value="newsletter">
                                        <label for="">Tela suave</label>
                                    </li>   
                                    <li>
                                        <input type="checkbox" id="" name="" value="newsletter">
                                        <label for="">Anti Hongos</label>
                                    </li>    
                                    <li>
                                        <input type="checkbox" id="" name="" value="newsletter">
                                        <label for="">Doble espuma</label>
                                    </li>       
                                </ul>
                            </div>
                            <div class="col-md-12"><button class="btn btn-primary">Buscar</button></div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="container padding-v--big">
                <div class="col-md-12">
                    <h3 class="title-dark--xl margin-b--sm">Te puede interesar</h3>
                </div>
                <div class="col-md-3 col-xs-6 product-item">
                        <a href="product.php" class="product-img">
                            <img src="assets/img/product.png">
                            <span class="promo-badge">20% OFF</span>
                        </a>
                        <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                        <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                        <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                    </div>
                    <div class="col-md-3 col-xs-6 product-item">
                        <a href="product.php" class="product-img">
                            <img src="assets/img/product.png">
                        </a>
                        <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                        <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                        <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                    </div>
                    <div class="col-md-3 col-xs-6 product-item">
                        <a href="product.php" class="product-img">
                            <img src="assets/img/product.png">
                        </a>
                        <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                        <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                        <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                    </div>
                    <div class="col-md-3 col-xs-6 product-item">
                        <a href="product.php" class="product-img">
                            <img src="assets/img/product.png">
                        </a>
                        <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                        <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                        <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                    </div>
            </div>
            <?php include('inc/footer.php');?>
            
        
    	</body>
    </html>