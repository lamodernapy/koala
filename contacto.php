    <!doctype html>
    <html>	
    	<?php include('inc/head.php');?>
    	<body>
            <?php include('inc/nav.php');?>
    		<div class="container padding-v--big">
                <div class="col-md-8 col-md-offset-2">
                    <h2 class="title-dark--xl">Contactanos</h2>
                    <span class="division">&nbsp;</span>
                    <p>Dejanos tu mensaje y nos comunicaremos contigo</p>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <label>Nombre y Apellido</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <label>Asunto</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <label>Teléfono</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label>Mensaje</label>
                                    <textarea class="form-control" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Enviar" class="btn btn-primary pull-right">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php include('inc/footer.php');?>
            
        
    	</body>
    </html>