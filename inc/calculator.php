<div class="calculator">
    <a href="javascript:void(0)" class="close">x</a>
    <div class="calculator-wrapper calc-step-1">
        <h3 class="title-blue--lg">Calculadora de Cuotas</h3>
        <span class="division">&nbsp;</span>
        <h5 class="title-dark--lg">¿Desea hacer una entrega inicial?</h5>
        <form class="">
            <ul class="inline-items">
                <li>
                    <a href="javascript:void(0)">Si</a>
                </li>
                <li>
                    <a href="javascript:void(0)">No</a>
                </li>
            </ul>
            <div class="input-group">
                <label>Producto</label>
                <select>
                    <option>Sommier Ultra Visco Pocket</option>
                    <option>Sommier Línea Clásica</option>
                    <option>Sommier Línea Juvenil</option>
                    <option>Sommier Platino</option>
                </select>
            </div>
            <div class="input-group">
                <label>Medida</label>
                <select>
                    <option>1,40 x 1,90 m</option>
                    <option>1,60 x 2 m</option>
                </select>
            </div>
            <div class="input-group">
                <label>Monto entrega inicial</label>
                <input type="text" class="form-control">
            </div>
            <div class="input-group">
                <label>Número de cuotas</label>
                <input type="text" class="form-control">
            </div>
            <input type="submit" class="btn btn-primary" value="Calcular">
        </form>
    </div>
    <!-- <div class="calculator-wrapper calc-step-2">
        <h3 class="title-blue--lg">Calculadora de Cuotas</h3>
        <span class="division">&nbsp;</span>
        <h5 class="title-dark--lg">Sommier Ultra Visco Pocket</h5>
        <h3 class="title-light--md">1,40 x 1,90 m</h3>
        <span class="division">&nbsp;</span>
        <form class="">
            <div class="input-group">
                <label>Monto entrega inicial</label>
                <input type="text" class="form-control">
            </div>
            <div class="input-group">
                <label>Número de cuotas</label>
                <input type="text" class="form-control">
            </div>
            
            <h5 class="title-dark--lg pull-left text-left"><small>Resultado</small><br>10 cuotas de Gs 400.000</h5>
            <div class="input-group">
                <input type="submit" class="btn btn-primary pull-left" value="Calcular">
                <input type="submit" class="btn btn-primary pull-right" value="Solicitar Asesor">
            </div>
        </form>
    </div> -->
</div>