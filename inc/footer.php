<footer>
	<div class="container">
		<div class="col-md-2 col-xs-6 margin-b--sm">
			<h5>Sobre Koala</h5>
			<a href="politicas-privacidad.php" class="link-white">Políticas de privacidad</a>
			<a href="sobre-nosotros.php" class="link-white">Sobre nosotros</a>
			<a href="sucursales.php" class="link-white">Sucursales</a>
			<a href="trabaja-con-nosotros.php" class="link-white">Trabajá con nosotros</a>
		</div>	
		<div class="col-md-2 col-xs-6 margin-b--sm">
			<h5>Atención al Cliente</h5>
			<a href="servicio-tecnico.php" class="link-white">Servicio Técnico</a>
			<a href="faq.php" class="link-white">¿Sabías qué?</a>
			<a href="solicitar-asesor.php" class="link-white">Solicitá un asesor</a>
		</div>	
		<div class="col-md-2 col-xs-12 margin-b--sm">
			<h5>Seguínos</h5>
			<a href="#"><img src="/assets/icons/facebook.png" width="20" style="margin-right: 10px;"></a>
			<a href="#"><img src="/assets/icons/instagram.png" width="20"></a>
		</div>	
		<div class="col-md-6 col-xs-12">
			<h5>Ofertas y Novedades</h5>
			<form class="form-inline">
			<input type="text" class="form-control margin-b--xs" id="" placeholder="Nombre">

			<input type="text" class="form-control margin-b--xs" id="" placeholder="Email o celular">
				
			<button type="submit" class="btn btn-primary margin-b--xs">Suscribirme</button>
			</div>

			</form>
		</div>
	</div>
	<div class="container copyright">
		<p class="text-center">© Koala 2017. Todos los derechos reservados.</p>
	</div>
</footer>