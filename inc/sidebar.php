<div class="sidebar col-sm-3 hidden-xs">
	<h5 class="title-bold--m">Categorías</h5>
	<a href="products.php" class="sidebar-item">
		<img src="assets/icons/promo-icon.png">
		Promoción del mes
	</a>
	<a href="products.php" class="sidebar-item">
		<img src="assets/icons/sommier-icon.png">
		Sommier
	</a>
	<a href="products.php" class="sidebar-item">
		<img src="assets/icons/cabeceras-icon.png">
		Cabeceras
	</a>
	<a href="products.php" class="sidebar-item">
		<img src="assets/icons/mesitas-icon.png">
		Mesitas
	</a>
	<a href="products.php" class="sidebar-item">
		<img src="assets/icons/almohadas-icon.png">
		Almohadas
	</a>
	<a href="products.php" class="sidebar-item">
		<img src="assets/icons/living-icon.png">
		Living
	</a>
	<a href="products.php" class="sidebar-item">
		<img src="assets/icons/blanqueria-icon.png">
		Blanquería
	</a>
	<!-- banners laterales promociones -->

	<a href="#" class="sidebar-banner">
		<img src="assets/img/sidebar-banner.png">
	</a>
</div>
<div class="col-xs-12 visible-xs margin-b--sm">
	<label>Categorías</label>
	<select>
		<option>Promoción del mes</option>
		<option>Sommier</option>
		<option>Cabeceras</option>
		<option>Mesitas</option>
		<option>Almohadas</option>
		<option>Living</option>
		<option>Blanquería</option>
	</select>
</div>