<head>
	<title>Koala</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" type="image/png" href="images/favicon.png">
	<link href="stylesheets/style.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    
	<script src="js/jquery-2.1.1.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cycle2.min.js"></script>

	<script type="text/javascript">
        $(window).scroll(function() {
          if ($(document).scrollTop() > 50) {
            $('.navbar').addClass('shrink');
          } else {
            $('.navbar').removeClass('shrink');
          }
        });

        function updateSpinner(obj)
        {
            var contentObj = document.getElementById("content");
            var value = parseInt(contentObj.value);
            if(obj.id == "down") {
                value--;
            } else {
                value++;
            }
            contentObj.value = value;
        };
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })
        $(document).ready(function() {
            $('.calculator-trigger').click(function(){
                $('.calculator').addClass('active');
                $('.overlay').addClass('active');
                $('body').addClass('no-scroll');
            }); 
            $('.calculator .close').click(function(){
                $('.calculator').removeClass('active');
                $('.overlay').removeClass('active');
                $('body').removeClass('no-scroll');
            }); 
        });


	</script>
</head>