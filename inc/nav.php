<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="navbar-mobile" data-target="#navbar-mobile">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="index.php"><img src="/assets/img/koala-logo.png"></a>
        </div>
        <div class="navbar-collapse collapse" id="navbar-collapsible">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden-md hidden-sm hidden-xs"><a href="index.php">Inicio</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="productos.php" role="button" aria-haspopup="true" aria-expanded="false">
                        Sommier <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="producto.php">Ultra Visco pocket</a></li>
                        <li><a href="producto.php">Platino</a></li>
                        <li><a href="producto.php">Ultrafirme</a></li>
                        <li><a href="producto.php">Premium</a></li>
                        <li><a href="producto.php">Clásica</a></li>
                        <li><a href="producto.php">Línea Juvenil</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="productos.php" role="button" aria-haspopup="true" aria-expanded="false">
                        Accesorios <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="producto.php">Cabeceras</a></li>
                        <li><a href="producto.php">Mesitas</a></li>
                        <li><a href="producto.php">Almohadas</a></li>
                        <li><a href="producto.php">Living</a></li>
                        <li><a href="producto.php">Blanquería</a></li>
                    </ul>
                </li>
                <li><a href="sucursales.php">Sucursales</a></li>
                <li><a href="contacto.php">Contacto</a></li>
                <li><a href="busqueda.php"><img src="assets/icons/search-icon.png" width="15"> </a></li>
            </ul>
        </div>

    </div>
</div>