<!doctype html>
<html>	
	<?php include('inc/head.php');?>
	<body>
        <?php include('inc/nav.php');?>
		<div class="container padding-v--big">
            <?php include('inc/sidebar.php');?>
        
            <div class="products-container col-sm-9">
                <div class="col-md-4 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                        <span class="promo-badge">20% OFF</span>
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-4 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-4 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-4 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-4 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-4 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-4 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-4 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
                <div class="col-md-4 col-xs-6 product-item">
                    <a href="product.php" class="product-img">
                        <img src="assets/img/product.png">  
                    </a>
                    <a href="product.php"><h4 class="product-title">Sommier Ultra Visco Pocket</h4></a>
                    <span class="product-price">Cuotas desde <strong>G.320.200</strong></span>
                    <a href="javascript:void(0)" class="calculator-trigger"><img src="assets/icons/calculadora-icon.png">Calcular Cuotas</a>
                </div>
            </div>
        </div>
        <div class="container padding-v--big">
            <span class="division">&nbsp;</span>
            <div class="banner-md col-md-4 col-xs-6 margin-b--sm">
                <div class="banner-md--content" style="background-image: url(assets/img/promo.png)">
                    <span class="text-uppercase">Destacado</span>
                    <h3 class="title-dark--lg">Ultra Visco Pocket</h3>
                    <a href="producto" class="link-uppercase">Ver más -></a>
                </div>
            </div>
            <div class="banner-md col-md-4 col-xs-6 margin-b--sm">
                <div class="banner-md--content" style="background-image: url(assets/img/promo.png)">
                    <span class="text-uppercase">Destacado</span>
                    <h3 class="title-dark--lg">Ultra Visco Pocket</h3>
                    <a href="producto" class="link-uppercase">Ver más -></a>
                </div>
            </div>
            <div class="banner-md col-md-4 hidden-xs  hidden-sm margin-b--sm">
                <div class="banner-md--content" style="background-image: url(assets/img/promo.png)">
                    <span class="text-uppercase">Destacado</span>
                    <h3 class="title-dark--lg">Ultra Visco Pocket</h3>
                    <a href="producto" class="link-uppercase">Ver más -></a>
                </div>
            </div>
        </div>
        <div class="container padding-v--big">
            
            <div class="col-md-8 col-md-offset-2 quick-links">
                <a href="solicitar-asesor.php" class="pull-left">
                    <img src="/assets/icons/asesor-icon.png" class="hidden-xs">
                    <strong>Solicitá un asesor de ventas</strong>
                    <span>Encontrá el producto que buscás</span>
                </a>
                <a href="#" class="pull-right">
                    <img src="/assets/icons/credito-icon.png" class="hidden-xs">
                    <strong>Requisitos para créditos</strong>
                    <span>Precios accesibles para todos</span>
                </a>
            </div>

        </div>
        <?php include('inc/footer.php');?>
        
        
        <div class="overlay"></div>
        <?php include('inc/calculator.php');?>
        
	</body>
</html>